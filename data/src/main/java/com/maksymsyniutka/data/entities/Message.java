package com.maksymsyniutka.data.entities;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 2:05 PM
 */
public class Message {
    private String body;
    private String from;

    public Message() {
    }

    public Message(String body, String from) {
        this.body = body;
        this.from = from;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }
}
