package com.maksymsyniutka.data.usecases;

import com.google.firebase.database.DatabaseReference;
import com.maksymsyniutka.data.entities.Message;
import com.maksymsyniutka.data.services.FirebaseChatService;

import java.util.List;

import durdinapps.rxfirebase2.RxFirebaseChildEvent;
import io.reactivex.Completable;
import io.reactivex.Flowable;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 1:31 PM
 */
public class FirebaseUseCase extends UseCase {

    private FirebaseChatService mChatService;
    private DatabaseReference mDatabaseReference;

    public FirebaseUseCase(FirebaseChatService chatService, DatabaseReference databaseReference) {
        mChatService = chatService;
        mDatabaseReference = databaseReference;
    }

    public Flowable<List<Message>> getChatHistory() {
        return mChatService.getChatHistory(mDatabaseReference);
    }

    public Flowable<RxFirebaseChildEvent<Message>> startObservingChanges() {
        return mChatService.startObservingChanges(mDatabaseReference);
    }

    public Completable sendMessage(Message message) {
        return mChatService.sendMessage(mDatabaseReference, message);
    }
}
