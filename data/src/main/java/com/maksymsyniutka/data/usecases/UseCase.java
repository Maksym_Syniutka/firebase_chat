package com.maksymsyniutka.data.usecases;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 1:31 PM
 */
public abstract class UseCase {
    protected final String TAG = this.getClass().getSimpleName();
}
