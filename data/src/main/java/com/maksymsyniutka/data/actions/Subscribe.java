package com.maksymsyniutka.data.actions;

import org.reactivestreams.Subscription;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 1:31 PM
 */
public interface Subscribe {

    void onSubscribe(Subscription s);

}