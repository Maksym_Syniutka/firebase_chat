package com.maksymsyniutka.data.actions;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 1:31 PM
 */
public interface Complete {

    void onComplete();

}