package com.maksymsyniutka.data.services;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.maksymsyniutka.data.entities.Message;

import java.util.List;

import durdinapps.rxfirebase2.DataSnapshotMapper;
import durdinapps.rxfirebase2.RxFirebaseChildEvent;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Completable;
import io.reactivex.Flowable;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 1:33 PM
 */
public class FirebaseChatServiceImpl implements FirebaseChatService {
    @Override
    public Flowable<List<Message>> getChatHistory(DatabaseReference databaseReference) {
        return RxFirebaseDatabase.observeValueEvent(
                databaseReference.child("messages"),
                DataSnapshotMapper.listOf(Message.class)
        );
    }

    @Override
    public Flowable<RxFirebaseChildEvent<Message>> startObservingChanges(DatabaseReference databaseReference) {
        return RxFirebaseDatabase.observeChildEvent(
                databaseReference.child("messages"),
                Message.class
        );
    }

    @Override
    public Completable sendMessage(DatabaseReference databaseReference, Message message) {
        return RxFirebaseDatabase.setValue(
                databaseReference.child("messages").child(String.valueOf(System.currentTimeMillis())),
                message);
    }
}
