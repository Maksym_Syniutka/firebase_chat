package com.maksymsyniutka.data.services;

import com.google.firebase.database.DatabaseReference;
import com.maksymsyniutka.data.entities.Message;

import java.util.List;

import durdinapps.rxfirebase2.RxFirebaseChildEvent;
import io.reactivex.Completable;
import io.reactivex.Flowable;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 1:33 PM
 */
public interface FirebaseChatService {

    Flowable<List<Message>> getChatHistory(DatabaseReference databaseReference);

    Flowable<RxFirebaseChildEvent<Message>> startObservingChanges(DatabaseReference databaseReference);

    Completable sendMessage(DatabaseReference databaseReference, Message message);
}
