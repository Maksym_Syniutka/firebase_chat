package com.maksymsyniutka.firebasechat.activities;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.maksymsyniutka.firebasechat.R;
import com.maksymsyniutka.firebasechat.contracts.BaseContract;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 2:33 PM
 */
public abstract class BaseActivity<Action extends BaseContract.Actions> extends AppCompatActivity
        implements BaseContract.View {

    @Inject
    Action mActions;

    private Unbinder mUnbinder;

    protected FrameLayout mBaseContentLayout;
    protected FrameLayout mLoadLayout;

    protected abstract void inject();

    protected abstract void releaseActivityComponent();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        inject();
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if (mActions != null) mActions.onCreate(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        setChildContentView(getLayoutInflater().inflate(layoutResID, null));
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (mActions != null) mActions.onViewAttached(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mActions != null) mActions.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mActions != null) mActions.onStop();
    }

    @Override
    public void onDetachedFromWindow() {
        if (mActions != null) mActions.onViewDetached();
        super.onDetachedFromWindow();
    }

    @Override
    protected void onDestroy() {
        if (mActions != null) {
            mActions.onViewDestroyed();
            mActions = null;
        }

        releaseActivityComponent();

        super.onDestroy();
    }

    @Override
    public void showLoading() {
        mLoadLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadLayout.setVisibility(View.GONE);
    }

    @Override
    public void showDialog(String title, String message) {

    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    protected void setChildContentView(View view) {
        View fullLayout = getLayoutInflater().inflate(R.layout.activity_base, null);
        mBaseContentLayout = fullLayout.findViewById(R.id.base_activity_base_content_layout);
        mLoadLayout = fullLayout.findViewById(R.id.base_activity_load_layout);
        if (view != null) mBaseContentLayout.addView(view);
        super.setContentView(fullLayout);
        mUnbinder = ButterKnife.bind(this, fullLayout);
    }
}
