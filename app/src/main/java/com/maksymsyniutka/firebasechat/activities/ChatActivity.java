package com.maksymsyniutka.firebasechat.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import com.maksymsyniutka.data.entities.Message;
import com.maksymsyniutka.firebasechat.App;
import com.maksymsyniutka.firebasechat.Constants;
import com.maksymsyniutka.firebasechat.R;
import com.maksymsyniutka.firebasechat.adapters.ChatRecyclerAdapter;
import com.maksymsyniutka.firebasechat.contracts.ChatActivityContract;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class ChatActivity extends BaseActivity<ChatActivityContract.Actions> implements ChatActivityContract.View {

    @BindView(R.id.chat_activity_messages_recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.chat_activity_message_etxt)
    EditText mMessageEtxt;

    private ChatRecyclerAdapter mAdapter;
    private String mUserId;

    public static void start(Activity activity,
                             String username,
                             boolean finishCurrentActivity) {
        Intent intent = new Intent(activity, ChatActivity.class);
        intent.putExtra(Constants.PassingData.USERNAME, username);
        activity.startActivity(intent);
        if (finishCurrentActivity) activity.finish();
    }

    @Override
    protected void inject() {
        ((App) getApplication()).getChatActivityComponent().inject(this);
    }

    @Override
    protected void releaseActivityComponent() {
        ((App) getApplication()).releaseChatActivityComponent();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);

        mUserId = getIntent().getStringExtra(Constants.PassingData.USERNAME);

        mAdapter = new ChatRecyclerAdapter(new ArrayList<>(), mUserId);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);

        mActions.startObservingChanges();
    }


    @Override
    public void onMessageAdded(Message message) {
        mAdapter.addMessage(message);
    }

    @Override
    public void onMessageRemoved(Message message) {

    }

    @Override
    public void onMessageChanged(Message message) {

    }

    @Override
    public void onMessageSent() {
        mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount());
    }

    @OnClick({R.id.chat_activity_send_btn})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chat_activity_send_btn:
                mActions.sendMessage(new Message(mMessageEtxt.getText().toString(), mUserId));
                mMessageEtxt.setText("");
                break;
        }
    }
}
