package com.maksymsyniutka.firebasechat.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.maksymsyniutka.firebasechat.App;
import com.maksymsyniutka.firebasechat.R;
import com.maksymsyniutka.firebasechat.RxTextWatcher;
import com.maksymsyniutka.firebasechat.contracts.SelectUsernameActivityContract;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class SelectUsernameActivity extends BaseActivity<SelectUsernameActivityContract.Actions> implements SelectUsernameActivityContract.Views {

    @BindView(R.id.select_username_activity_username_etxt)
    EditText mUsernameEtxt;
    @BindView(R.id.select_username_activity_next_btn)
    Button mNextBtn;

    @Override
    protected void inject() {
        ((App) getApplication()).getUsernameActivityComponent().inject(this);
    }

    @Override
    protected void releaseActivityComponent() {
        ((App) getApplication()).releaseSelectUsernameActivityComponent();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_username);

        RxTextWatcher.from(mUsernameEtxt)
                .doOnNext(s -> mNextBtn.setEnabled(!TextUtils.isEmpty(s)))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    @OnClick({R.id.select_username_activity_next_btn})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.select_username_activity_next_btn:
                ChatActivity.start(this, mUsernameEtxt.getText().toString(), true);
                break;
        }
    }
}
