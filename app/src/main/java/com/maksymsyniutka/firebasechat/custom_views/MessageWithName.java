package com.maksymsyniutka.firebasechat.custom_views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.IntDef;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.util.AttributeSet;
import android.widget.TextView;

import com.maksymsyniutka.firebasechat.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Maksym Syniutka
 * Date: 30-Jun-18
 * Time: 12:31 PM
 */
public class MessageWithName extends ConstraintLayout {

    @BindView(R.id.widget_message_with_name_user_name_txt)
    TextView mNameTxt;
    @BindView(R.id.widget_message_with_name_message_txt)
    TextView mMessageContentTxt;

    public MessageWithName(Context context) {
        super(context);
    }

    public MessageWithName(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public MessageWithName(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void setMessageBackground(int color) {
        mMessageContentTxt.setBackgroundColor(color);
    }

    public void setMessagePosition(@MessagePosition int position) {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this);
        switch (position) {
            case MessagePosition.END:
                constraintSet.clear(mNameTxt.getId(), ConstraintSet.START);
                constraintSet.clear(mMessageContentTxt.getId(), ConstraintSet.START);

                constraintSet.connect(mNameTxt.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END, 8);
                constraintSet.connect(mMessageContentTxt.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END, 8);
                break;
            case MessagePosition.START:
                constraintSet.clear(mNameTxt.getId(), ConstraintSet.END);
                constraintSet.clear(mMessageContentTxt.getId(), ConstraintSet.END);

                constraintSet.connect(mNameTxt.getId(), ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, 8);
                constraintSet.connect(mMessageContentTxt.getId(), ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, 8);
                break;
        }
        constraintSet.applyTo(this);
    }

    public void setMessage(String message) {
        mMessageContentTxt.setText(message);
    }

    public void setUserName(String userName) {
        mNameTxt.setText(userName);
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.widget_message_with_name, this);
        ButterKnife.bind(this);

        TypedArray ta = context
                .getTheme()
                .obtainStyledAttributes(attrs, R.styleable.MessageWithName, 0, 0);

        setMessageBackground(ta.getColor(R.styleable.MessageWithName_message_background, Color.GREEN));
        setMessagePosition(ta.getInt(R.styleable.MessageWithName_message_position, MessagePosition.START));

        ta.recycle();
    }

    @IntDef({
            MessagePosition.START,
            MessagePosition.END
    })
    public @interface MessagePosition {
        int START = 0;
        int END = 1;
    }
}
