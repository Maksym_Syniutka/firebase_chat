package com.maksymsyniutka.firebasechat;

import android.support.multidex.MultiDexApplication;

import com.maksymsyniutka.firebasechat.di.components.AppComponent;
import com.maksymsyniutka.firebasechat.di.components.ChatActivityComponent;
import com.maksymsyniutka.firebasechat.di.components.DaggerAppComponent;
import com.maksymsyniutka.firebasechat.di.components.SelectUsernameActivityComponent;
import com.maksymsyniutka.firebasechat.di.modules.AppModule;
import com.maksymsyniutka.firebasechat.di.modules.activities.ChatActivityModule;
import com.maksymsyniutka.firebasechat.di.modules.activities.SelectUsernameActivityModule;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 3:02 PM
 */
public class App extends MultiDexApplication {

    private AppComponent mAppComponent;
    private ChatActivityComponent mChatActivityComponent;
    private SelectUsernameActivityComponent mUsernameActivityComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initializeInjector();
    }

    public AppComponent getAppComponent() {
        if (mAppComponent == null) {
            initializeInjector();
        }
        return mAppComponent;
    }

    public void releaseAppComponent() {
        mAppComponent = null;
    }

    public ChatActivityComponent getChatActivityComponent() {
        if (mChatActivityComponent == null) {
            mChatActivityComponent = getAppComponent().addChatActivityComponent(new ChatActivityModule());
        }
        return mChatActivityComponent;
    }

    public void releaseChatActivityComponent() {
        mChatActivityComponent = null;
    }

    public SelectUsernameActivityComponent getUsernameActivityComponent() {
        if (mUsernameActivityComponent == null) {
            mUsernameActivityComponent = getAppComponent().addSelectUsernameActivityComponent(new SelectUsernameActivityModule());
        }
        return mUsernameActivityComponent;
    }

    public void releaseSelectUsernameActivityComponent() {
        mUsernameActivityComponent = null;
    }


    private void initializeInjector() {
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }
}
