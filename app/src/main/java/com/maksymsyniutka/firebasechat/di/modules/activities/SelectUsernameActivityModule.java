package com.maksymsyniutka.firebasechat.di.modules.activities;

import android.app.Application;
import android.support.annotation.NonNull;

import com.maksymsyniutka.firebasechat.contracts.SelectUsernameActivityContract;
import com.maksymsyniutka.firebasechat.di.scopes.ChatActivityScope;
import com.maksymsyniutka.firebasechat.di.scopes.SelectUsernameScope;
import com.maksymsyniutka.firebasechat.presenters.SelectUsernameActivityPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * @author Maksym Syniutka
 * Date: 30-Jun-18
 * Time: 1:38 PM
 */
@Module
public class SelectUsernameActivityModule {

    @Provides
    @NonNull
    @SelectUsernameScope
    SelectUsernameActivityContract.Actions provideSelectUsernameActivityActions(Application application) {
        return new SelectUsernameActivityPresenter(application);
    }
}
