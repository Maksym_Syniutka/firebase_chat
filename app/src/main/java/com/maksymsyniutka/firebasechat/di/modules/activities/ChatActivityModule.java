package com.maksymsyniutka.firebasechat.di.modules.activities;

import android.app.Application;
import android.support.annotation.NonNull;

import com.maksymsyniutka.domain.interfaces.ChatInteractor;
import com.maksymsyniutka.firebasechat.contracts.ChatActivityContract;
import com.maksymsyniutka.firebasechat.di.scopes.ChatActivityScope;
import com.maksymsyniutka.firebasechat.presenters.ChatActivityPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 2:54 PM
 */
@Module
public class ChatActivityModule {

    @Provides
    @NonNull
    @ChatActivityScope
    ChatActivityContract.Actions provideChatActivityActions(Application application, ChatInteractor chatInteractor) {
        return new ChatActivityPresenter(application, chatInteractor);
    }
}
