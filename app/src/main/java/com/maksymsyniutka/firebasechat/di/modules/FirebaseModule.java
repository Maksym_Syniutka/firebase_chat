package com.maksymsyniutka.firebasechat.di.modules;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.maksymsyniutka.data.services.FirebaseChatService;
import com.maksymsyniutka.data.services.FirebaseChatServiceImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 2:30 PM
 */
@Module
public class FirebaseModule {

    @Provides
    @Singleton
    FirebaseChatService provideFirebaseChatService() {
        return new FirebaseChatServiceImpl();
    }

    @Provides
    @Singleton
    DatabaseReference provideDatabaseReference() {
        return FirebaseDatabase.getInstance().getReference();
    }
}
