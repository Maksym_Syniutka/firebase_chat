package com.maksymsyniutka.firebasechat.di.modules;

import com.google.firebase.database.DatabaseReference;
import com.maksymsyniutka.data.services.FirebaseChatService;
import com.maksymsyniutka.data.usecases.FirebaseUseCase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 2:25 PM
 */
@Module
public class UseCaseModule {

    @Provides
    @Singleton
    FirebaseUseCase provideFirebaseUC(FirebaseChatService firebaseChatService, DatabaseReference databaseReference) {
        return new FirebaseUseCase(firebaseChatService, databaseReference);
    }
}
