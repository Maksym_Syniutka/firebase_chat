package com.maksymsyniutka.firebasechat.di.components;

import android.content.Context;

import com.maksymsyniutka.firebasechat.di.modules.AppModule;
import com.maksymsyniutka.firebasechat.di.modules.FirebaseModule;
import com.maksymsyniutka.firebasechat.di.modules.InteractorModule;
import com.maksymsyniutka.firebasechat.di.modules.UseCaseModule;
import com.maksymsyniutka.firebasechat.di.modules.activities.ChatActivityModule;
import com.maksymsyniutka.firebasechat.di.modules.activities.SelectUsernameActivityModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 2:26 PM
 */
@Component(modules = {
        AppModule.class,
        UseCaseModule.class,
        FirebaseModule.class,
        InteractorModule.class
})
@Singleton
public interface AppComponent {

    ChatActivityComponent addChatActivityComponent(ChatActivityModule module);

    SelectUsernameActivityComponent addSelectUsernameActivityComponent(SelectUsernameActivityModule module);

    Context context();

}
