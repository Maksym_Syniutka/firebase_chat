package com.maksymsyniutka.firebasechat.di.scopes;

/**
 * @author Maksym Syniutka
 * Date: 30-Jun-18
 * Time: 1:39 PM
 */
public @interface SelectUsernameScope {
}
