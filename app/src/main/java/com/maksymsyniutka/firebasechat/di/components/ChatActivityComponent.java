package com.maksymsyniutka.firebasechat.di.components;

import com.maksymsyniutka.firebasechat.activities.ChatActivity;
import com.maksymsyniutka.firebasechat.di.modules.activities.ChatActivityModule;
import com.maksymsyniutka.firebasechat.di.scopes.ChatActivityScope;

import dagger.Subcomponent;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 2:27 PM
 */
@Subcomponent(modules = {ChatActivityModule.class})
@ChatActivityScope
public interface ChatActivityComponent {
    void inject(ChatActivity chatActivity);
}
