package com.maksymsyniutka.firebasechat.di.modules;

import android.app.Application;

import com.maksymsyniutka.data.usecases.FirebaseUseCase;
import com.maksymsyniutka.domain.implementations.ChatInteractorImpl;
import com.maksymsyniutka.domain.interfaces.ChatInteractor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 3:08 PM
 */
@Module
public class InteractorModule {

    @Provides
    @Singleton
    ChatInteractor provideChatInteractor(Application application, FirebaseUseCase firebaseUseCase) {
        return new ChatInteractorImpl(application, firebaseUseCase);
    }
}
