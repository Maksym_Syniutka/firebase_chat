package com.maksymsyniutka.firebasechat.di.components;

import com.maksymsyniutka.firebasechat.activities.SelectUsernameActivity;
import com.maksymsyniutka.firebasechat.di.modules.activities.SelectUsernameActivityModule;
import com.maksymsyniutka.firebasechat.di.scopes.SelectUsernameScope;

import dagger.Subcomponent;

/**
 * @author Maksym Syniutka
 * Date: 30-Jun-18
 * Time: 1:39 PM
 */
@Subcomponent(modules = {SelectUsernameActivityModule.class})
@SelectUsernameScope
public interface SelectUsernameActivityComponent {
    void inject(SelectUsernameActivity activity);
}
