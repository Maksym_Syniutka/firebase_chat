package com.maksymsyniutka.firebasechat.di.scopes;

import javax.inject.Scope;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 2:27 PM
 */
@Scope
public @interface ChatActivityScope {
}
