package com.maksymsyniutka.firebasechat;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.MainThreadDisposable;

/**
 * @author Maksym Syniutka
 * Date: 30-Jun-18
 * Time: 2:01 PM
 */
public class RxTextWatcher {
    public static Observable<String> from(TextView view) {
        return new TextWatcherTextObservable(view);
    }

    private static class TextWatcherTextObservable extends Observable<String> {

        final TextView view;

        TextWatcherTextObservable(TextView view) {
            this.view = view;
        }

        @Override
        protected void subscribeActual(Observer<? super String> observer) {
            Listener listener = new Listener(view, observer);
            observer.onSubscribe(listener);
            view.addTextChangedListener(listener);
        }

        static final class Listener extends MainThreadDisposable implements TextWatcher {
            private final TextView view;
            private final Observer<? super String> observer;

            Listener(TextView view, Observer<? super String> observer) {
                this.view = view;
                this.observer = observer;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!isDisposed()) observer.onNext(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            protected void onDispose() {
                view.removeTextChangedListener(this);
            }
        }

    }

}
