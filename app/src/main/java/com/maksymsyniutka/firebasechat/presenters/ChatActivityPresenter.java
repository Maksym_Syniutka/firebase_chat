package com.maksymsyniutka.firebasechat.presenters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.maksymsyniutka.data.entities.Message;
import com.maksymsyniutka.domain.interfaces.ChatInteractor;
import com.maksymsyniutka.firebasechat.R;
import com.maksymsyniutka.firebasechat.contracts.ChatActivityContract;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 2:57 PM
 */
public class ChatActivityPresenter extends BasePresenter<ChatActivityContract.View> implements ChatActivityContract.Actions {

    private ChatInteractor mChatInteractor;

    public ChatActivityPresenter(@NonNull Context appContext, ChatInteractor chatInteractor) {
        super(appContext);
        mChatInteractor = chatInteractor;
    }

    @Override
    public void startObservingChanges() {
        mChatInteractor.startObservingChanges(
                next -> {
                    Log.d(TAG, "on event:" + next);
                    switch (next.getEventType()) {
                        case ADDED:
                            if (isViewAttached()) mView.onMessageAdded(next.getValue());
                            break;
                        case CHANGED:
                            if (isViewAttached()) mView.onMessageChanged(next.getValue());
                            break;
                        case REMOVED:
                            if (isViewAttached()) mView.onMessageRemoved(next.getValue());
                            break;
                    }
                }, error -> {
                    Log.d(TAG, "error on event: " + error);
                }
        );
    }

    @Override
    public void sendMessage(Message message) {
        if (TextUtils.isEmpty(message.getBody())) {
            if (isViewAttached()) {
                mView.showToast(mAppContext.getString(R.string.chat_activity_message_cannot_be_empty));
            }
            return;
        }

        if (isViewAttached()) mView.showLoading();

        mChatInteractor.sendMessage(
                () -> {
                    if (isViewAttached()) {
                        mView.hideLoading();
                        mView.onMessageSent();
                    }
                }, error -> {
                    if (isViewAttached()) {
                        mView.hideLoading();
                        mView.showToast(mAppContext.getString(R.string.chat_activity_error_sending_message));
                    }
                }, message
        );
    }
}
