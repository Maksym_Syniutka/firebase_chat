package com.maksymsyniutka.firebasechat.presenters;

import android.content.Context;
import android.support.annotation.NonNull;

import com.maksymsyniutka.firebasechat.contracts.SelectUsernameActivityContract;

/**
 * @author Maksym Syniutka
 * Date: 30-Jun-18
 * Time: 1:37 PM
 */
public class SelectUsernameActivityPresenter extends BasePresenter<SelectUsernameActivityContract.Views> implements SelectUsernameActivityContract.Actions {
    public SelectUsernameActivityPresenter(@NonNull Context appContext) {
        super(appContext);
    }
}
