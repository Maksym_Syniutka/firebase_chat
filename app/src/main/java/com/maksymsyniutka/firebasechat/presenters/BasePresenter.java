package com.maksymsyniutka.firebasechat.presenters;

import android.content.Context;
import android.support.annotation.NonNull;

import com.maksymsyniutka.firebasechat.contracts.BaseContract;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 2:56 PM
 */
public class BasePresenter<V extends BaseContract.View>
        implements BaseContract.Actions {
    protected final String TAG = this.getClass().getSimpleName();

    protected Context mAppContext;
    protected V mView;

    public BasePresenter(@NonNull Context appContext) {
        mAppContext = appContext.getApplicationContext();
    }

    @Override
    public <View extends BaseContract.View> void onViewAttached(View view) {
        mView = (V) view;
    }

    @Override
    public <View extends BaseContract.View> void onCreate(View view) {
        mView = (V) view;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onViewDetached() {
        mView = null;
    }

    @Override
    public void onViewDestroyed() {
        mView = null;
    }

    /**
     * Always use after async methods
     *
     * @return true if view is attached to presenter
     */
    public boolean isViewAttached() {
        return mView != null;
    }

    protected String getPresenterKey() {
        return TAG + this.hashCode();
    }
}
