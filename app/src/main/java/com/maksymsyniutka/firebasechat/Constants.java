package com.maksymsyniutka.firebasechat;

/**
 * @author Maksym Syniutka
 * Date: 30-Jun-18
 * Time: 1:57 PM
 */
public interface Constants {

    public interface PassingData {
        String USERNAME = "username";
    }
}
