package com.maksymsyniutka.firebasechat.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.maksymsyniutka.data.entities.Message;
import com.maksymsyniutka.firebasechat.R;
import com.maksymsyniutka.firebasechat.custom_views.MessageWithName;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 4:08 PM
 */
public class ChatRecyclerAdapter extends RecyclerView.Adapter<ChatRecyclerAdapter.ChatViewHolder> {

    private List<Message> mData;
    private String mUserId;

    public ChatRecyclerAdapter(@NonNull List<Message> messages, String userId) {
        mData = messages;
        mUserId = userId;
    }

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ChatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder holder, int position) {
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mData.get(position).getFrom().equals(mUserId)) {
            return R.layout.item_my_message;
        } else {
            return R.layout.item_other_user_message;
        }
    }

    public void addMessage(Message message) {
        mData.add(message);
        notifyItemInserted(mData.size());
    }

    class ChatViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_message_content_txt)
        MessageWithName mContentTxt;

        public ChatViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(Message message) {
            mContentTxt.setMessage(message.getBody());
            mContentTxt.setUserName(message.getFrom());
        }
    }
}
