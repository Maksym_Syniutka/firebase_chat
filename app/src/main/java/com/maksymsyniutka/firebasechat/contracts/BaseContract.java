package com.maksymsyniutka.firebasechat.contracts;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 2:33 PM
 */
public abstract class BaseContract {

    public interface Actions {
        <V extends View> void onCreate(V view);

        <V extends View> void onViewAttached(V view);

        void onStart();

        void onStop();

        void onViewDetached();

        void onViewDestroyed();

    }

    public interface View {
        void showLoading();

        void hideLoading();

        /**
         * Show an dialog
         *
         * @param title   A string representing an dialog title.
         * @param message A string representing an info.
         */
        void showDialog(String title, String message);

        void showToast(String message);
    }
}
