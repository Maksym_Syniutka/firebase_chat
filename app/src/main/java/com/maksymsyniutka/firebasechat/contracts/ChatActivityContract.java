package com.maksymsyniutka.firebasechat.contracts;

import com.maksymsyniutka.data.entities.Message;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 2:38 PM
 */
public class ChatActivityContract {

    public interface Actions extends BaseContract.Actions {
        void startObservingChanges();

        void sendMessage(Message message);
    }

    public interface View extends BaseContract.View {
        void onMessageAdded(Message message);

        void onMessageRemoved(Message message);

        void onMessageChanged(Message message);

        void onMessageSent();
    }
}
