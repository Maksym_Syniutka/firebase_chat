package com.maksymsyniutka.firebasechat.contracts;

/**
 * @author Maksym Syniutka
 * Date: 30-Jun-18
 * Time: 1:35 PM
 */
public class SelectUsernameActivityContract {

    public interface Actions extends BaseContract.Actions {

    }

    public interface Views extends BaseContract.View {

    }
}
