package com.maksymsyniutka.domain.implementations;

import android.content.Context;
import android.util.Log;

import com.maksymsyniutka.data.actions.Complete;
import com.maksymsyniutka.data.actions.Error;
import com.maksymsyniutka.data.actions.Next;
import com.maksymsyniutka.data.actions.Subscribe;
import com.maksymsyniutka.domain.interfaces.BaseInteractor;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 1:24 PM
 */
public class BaseInteractorImpl implements BaseInteractor {
    protected final String TAG = this.getClass().getSimpleName();

    protected Context mAppContext;
    protected List<Subscription> mSubscriptions;
    protected List<Disposable> mDisposables;

    public BaseInteractorImpl(Context appContext) {
        mAppContext = appContext.getApplicationContext();
    }

    @Override
    public void release() {
        if (mSubscriptions != null) {
            for (Subscription subscription : mSubscriptions) {
                subscription.cancel();
            }
            mSubscriptions.clear();
        }
        if (mDisposables != null) {
            for (Disposable disposable : mDisposables) {
                disposable.dispose();
            }
            mDisposables.clear();
        }
    }

    public void addSubscriptions(Subscription subscription) {
        if (mSubscriptions == null) {
            mSubscriptions = new ArrayList<>();
        }
        mSubscriptions.add(subscription);
    }

    public void addDisposable(Disposable disposable) {
        if (mDisposables == null) mDisposables = new ArrayList<>();
        mDisposables.add(disposable);
    }

    protected CompletableObserver createCompletable(Complete complete, Error error) {
        return new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {
                addDisposable(d);
            }

            @Override
            public void onComplete() {
                complete.onComplete();
            }

            @Override
            public void onError(Throwable e) {
                if (error != null) error.onError(e);
            }
        };
    }

    protected <T> Subscriber<T> createSubscriber(@NonNull Subscribe subscribe, Next<T> next, Error error) {
        return createSubscriber(subscribe, next, error, null);
    }

    protected <T> Subscriber<T> createSubscriber(@NonNull Subscribe subscribe, Next<T> next, Error error, Complete complete) {
        return new Subscriber<T>() {
            @Override
            public void onSubscribe(Subscription s) {
                addSubscriptions(s);
                subscribe.onSubscribe(s);
            }

            @Override
            public void onNext(T data) {
                if (next != null) next.onNext(data);
            }

            @Override
            public void onError(Throwable throwable) {
                Log.d(TAG, "createSubscriber -> onError: " + throwable);

                error.onError(throwable);
            }

            @Override
            public void onComplete() {
                if (complete != null) complete.onComplete();
            }
        };
    }

}
