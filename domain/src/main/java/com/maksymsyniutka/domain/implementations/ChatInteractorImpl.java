package com.maksymsyniutka.domain.implementations;

import android.content.Context;

import com.maksymsyniutka.data.actions.Complete;
import com.maksymsyniutka.data.actions.Error;
import com.maksymsyniutka.data.actions.Next;
import com.maksymsyniutka.data.entities.Message;
import com.maksymsyniutka.data.usecases.FirebaseUseCase;
import com.maksymsyniutka.domain.interfaces.ChatInteractor;

import java.util.List;

import durdinapps.rxfirebase2.RxFirebaseChildEvent;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 3:09 PM
 */
public class ChatInteractorImpl extends BaseInteractorImpl implements ChatInteractor {

    private FirebaseUseCase mFirebaseUseCase;

    public ChatInteractorImpl(Context appContext, FirebaseUseCase firebaseUseCase) {
        super(appContext);
        mFirebaseUseCase = firebaseUseCase;
    }

    @Override
    public void getChatHistory(Next<List<Message>> next, Error error) {
        mFirebaseUseCase.getChatHistory()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createSubscriber(s -> s.request(1), next, error));
    }

    @Override
    public void startObservingChanges(Next<RxFirebaseChildEvent<Message>> next, Error error) {
        mFirebaseUseCase.startObservingChanges()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createSubscriber(s -> s.request(Long.MAX_VALUE), next, error));
    }

    @Override
    public void sendMessage(Complete complete, Error error, Message message) {
        mFirebaseUseCase.sendMessage(message)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createCompletable(complete, error));
    }
}
