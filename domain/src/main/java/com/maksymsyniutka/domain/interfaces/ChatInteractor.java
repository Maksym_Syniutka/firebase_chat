package com.maksymsyniutka.domain.interfaces;

import com.maksymsyniutka.data.actions.Complete;
import com.maksymsyniutka.data.actions.Error;
import com.maksymsyniutka.data.actions.Next;
import com.maksymsyniutka.data.entities.Message;

import java.util.List;

import durdinapps.rxfirebase2.RxFirebaseChildEvent;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 1:29 PM
 */
public interface ChatInteractor {
    void getChatHistory(Next<List<Message>> next, Error error);

    void startObservingChanges(Next<RxFirebaseChildEvent<Message>> next, Error error);

    void sendMessage(Complete complete, Error error, Message message);
}
