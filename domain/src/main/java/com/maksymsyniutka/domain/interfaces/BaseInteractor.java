package com.maksymsyniutka.domain.interfaces;

/**
 * @author Maksym Syniutka
 * Date: 29-Jun-18
 * Time: 1:23 PM
 */
public interface BaseInteractor {
    void release();
}
